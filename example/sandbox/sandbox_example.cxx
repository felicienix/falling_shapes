#include "sandbox_example.hpp"
#include "polygon.hpp"
#include "physical_circle.hpp"
#include "physical_aabb.hpp"
#include <numeric>
#include <iostream>
#include <random>

#define WIDTH 1500
#define HEIGHT 900



namespace
{
	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();


	static float randomFloat(float min, float max)
	{
		std::uniform_real_distribution<> distr(min, max);
		return distr(RandomEngine);
	}
}

Sandbox::Sandbox()
    :fWindow(sf::VideoMode(WIDTH, HEIGHT), "Bouncing Shapes", sf::Style::Close),
    fFrameRate(sf::seconds(1.f/60.f)), fRealFrameRateText(), fEngine(6),
    fAttachedObj(nullptr), mousePositionBefore(), fFpsFont()
{
    fEngine.setPixelToMetersConversionFactor(1e-2f);
    fEngine.fGravityConstant *= 2.f;
    if(!fFpsFont.loadFromFile("resources/fonts/calibri.ttf"))
        throw std::runtime_error("couldn't load resources/fonts/calibri.ttf (maybe it doesn't exist)");

    fRealFrameRateText.setFont(fFpsFont);
    fRealFrameRateText.setCharacterSize(10U);
    fRealFrameRateText.setPosition(5.f, 5.f);
    fRealFrameRateText.setFillColor(sf::Color::Black);

    createRenderWindowPhysicalBounds();
}

void Sandbox::createRenderWindowPhysicalBounds()
{
    float half_w = 25.f;
    //floor
    PhysicalAABB* floor_poly = new PhysicalAABB(WIDTH, 2.f * half_w);
    floor_poly->setPosition(WIDTH * 0.5f, HEIGHT - half_w);
    floor_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    floor_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(floor_poly);

    //ceiling
    PhysicalAABB* ceiling_poly = new PhysicalAABB(WIDTH, 2.f * half_w);
    ceiling_poly->setPosition(WIDTH * 0.5f, half_w);
    ceiling_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    ceiling_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(ceiling_poly);

    //wall on the right side
    PhysicalAABB* right_poly = new PhysicalAABB(2.f * half_w, HEIGHT);
    right_poly->setPosition(WIDTH - half_w, HEIGHT * 0.5f);
    right_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    right_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(right_poly);

    //wall on the left side
    PhysicalAABB* left_poly = new PhysicalAABB(2.f * half_w, HEIGHT);
    left_poly->setPosition(half_w, HEIGHT * 0.5f);
    left_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    left_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(left_poly);
}

Sandbox::~Sandbox()
{
}

void Sandbox::run()
{
    sf::Clock appClock;
    sf::Time accu = sf::Time::Zero, deltaTime = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;

    while(fWindow.isOpen())
    {
        deltaTime = appClock.restart();
        accu += deltaTime;
        realFrameRateAccu += deltaTime;
        //update the application
        while(accu >= fFrameRate)
        {
            accu -= fFrameRate;

            handleEvent();
            update();

        } 

        //update the real frame rate displaying text
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fRealFrameRateText.setString(std::to_string((int)(1 / deltaTime.asSeconds())) + " fps\n" + std::to_string(deltaTime.asMicroseconds()) + "us");
        }
        
        render();
    }
}

void Sandbox::keyEvents(const sf::Event& event)
{
    sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
    switch (event.key.code)
    {
    case sf::Keyboard::Escape:
        fWindow.close();
        break;
    case sf::Keyboard::C:
        add_circle(mouse_pos.x, mouse_pos.y);
    break;
    case sf::Keyboard::S:
        add_poly(mouse_pos.x, mouse_pos.y, 4);
    break;
    case sf::Keyboard::P:
        add_poly(mouse_pos.x, mouse_pos.y, 5);
    break;
    case sf::Keyboard::X:
        add_poly(mouse_pos.x, mouse_pos.y, 6);
    break;
    case sf::Keyboard::A:
        add_aabb(mouse_pos.x, mouse_pos.y);
    break;
    case sf::Keyboard::Space:
        cloneAttachedShape(mouse_pos);
    break;
    default:
        break;
    }

}

void Sandbox::cloneAttachedShape(const sf::Vector2f& mouse_pos)
{
    //first check that the mouse is in the application's window
    if(mouse_pos.x < WIDTH && mouse_pos.y <= HEIGHT 
        && mouse_pos.x > 0.f && mouse_pos.y > 0.f)
    {
        if(fAttachedObj)
        {

            PhysicalObject* new_obj = fAttachedObj->clonePhysicalObject();
            new_obj->fGravitySensitive = true;

            fEngine.addObject(new_obj);
        
        }
    }
}

void Sandbox::attachObjectToCursor(PhysicalObject* obj)
{
    fAttachedObj = obj;
    fEngine.removeGravityOnObject(obj);
    obj->fLinearVelocity = sf::Vector2f();
    obj->setPosition(sf::Vector2f(sf::Mouse::getPosition(fWindow)));
}

void Sandbox::handleEvent()
{
    sf::Event event;
    while(fWindow.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            fWindow.close();
            break;
        case sf::Event::KeyPressed:
            keyEvents(event);
            break;
        break;
        case sf::Event::MouseButtonPressed:
            isObjAttached = !isObjAttached;
            if(isObjAttached)   //if the user wants to grab an object
            {
                sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
                if(mouse_pos.x < WIDTH && mouse_pos.y <= HEIGHT 
                && mouse_pos.x > 0.f && mouse_pos.y > 0.f)  //if the mouse is in the application's window
                {
                    for(auto* obj : fEngine)    //check if the cursor is on one object and if so attach him to it
                    {
                        if(obj->fGravitySensitive && obj->getBoundingRectangle().contains(mouse_pos))
                        {
                            attachObjectToCursor(obj);
                            break;
                        }
                    }
                }
            }
            else if(fAttachedObj)   //if the user wants to let go of the attached object
            {
                fEngine.addGravityOnObject(fAttachedObj);
                fAttachedObj = nullptr;
            }
        break;
        default:
            break;
        }
    }
}


void Sandbox::add_poly(float posx, float posy, int sides)
{
    Polygon* poly = new Polygon(50.f, sides, 1.f);
    poly->setPosition(posx, posy);
    poly->computeCentroid();
    poly->setMass(2.f);
    poly->fRestitutionCoeff = .5f;
    poly->fInitialRestCoeff = poly->fRestitutionCoeff;
    poly->fGravitySensitive = true;
    poly->rotate(45.f);

    fEngine.addObject(poly);
}

void Sandbox::add_circle(float posx, float posy)
{
    PhysicalCircle* circle = new PhysicalCircle(50.f);
    circle->setPosition(posx, posy);
    circle->setMass(.5f);
    circle->fRestitutionCoeff = .5f;
    circle->fInitialRestCoeff = circle->fRestitutionCoeff;
    circle->fGravitySensitive = true;

    fEngine.addObject(circle);   
}

void Sandbox::add_aabb(float posx, float posy)
{
    PhysicalAABB* aabb = new PhysicalAABB(randomFloat(10.f, 100.f), randomFloat(10.f, 100.f));
    aabb->setPosition(posx, posy);
    aabb->setMass(1.f);
    aabb->fRestitutionCoeff = 1.1f;
    aabb->fInitialRestCoeff = aabb->fRestitutionCoeff;
    aabb->fGravitySensitive = true;

    fEngine.addObject(aabb);   
}

void Sandbox::update()
{
    if(fAttachedObj)
    {
        sf::Vector2f current_mousePos(sf::Mouse::getPosition(fWindow));
        fAttachedObj->fLinearVelocity = (current_mousePos - mousePositionBefore) / fFrameRate.asSeconds();
        fAttachedObj->setPosition(current_mousePos);
    }
    fEngine.update(fFrameRate);


    mousePositionBefore = sf::Vector2f(sf::Mouse::getPosition(fWindow));
}

void Sandbox::render()
{
    fWindow.clear(sf::Color::White);

    for(auto physicalShape : fEngine)
    {
        fWindow.draw(*(physicalShape->getVisualMesh().get()));
    }

    fWindow.draw(fRealFrameRateText);
    fWindow.display();
}


int main(int argc, char** argv)
{
    try
    {
        Sandbox appli;

        appli.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    return 0;
}
#ifndef SANDBOX_HPP
#define SANDBOX_HPP
#include "SFML/Graphics.hpp"
#include "physics_engine.hpp"

class Sandbox
{

public:
    Sandbox();
    ~Sandbox();

    void run();

private:

    sf::RenderWindow fWindow;
    sf::Time fFrameRate;
    
    sf::Font fFpsFont;
    sf::Text fRealFrameRateText;

    PhysicsEngine fEngine;
    PhysicalObject* fAttachedObj;
    bool isObjAttached = false;
    sf::Vector2f mousePositionBefore;

    void attachObjectToCursor(PhysicalObject* obj);
    void add_poly(float posx, float posy, int sides);
    void add_circle(float posx, float posy);
    void add_aabb(float posx, float posy);
    void cloneAttachedShape(const sf::Vector2f& mouse_pos);

    void createRenderWindowPhysicalBounds();

    void update();
    void handleEvent();
    void render();

    void keyEvents(const sf::Event& event);

};

#endif //SANDBOX_HPP
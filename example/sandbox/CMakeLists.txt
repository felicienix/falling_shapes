cmake_minimum_required(VERSION 3.0)
project("sandbox_example")

set(SFML_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../SFML-2.5.1)

set(SFML_DIR ${SFML_PATH}/lib/cmake/SFML)

set(AlphEngine2D_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../AlphEngine2D/lib/cmake)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_SOURCE_DIR})

find_package(SFML 2.5 COMPONENTS window audio REQUIRED)
find_package(AlphEngine2D 1.0.0 REQUIRED)

add_executable(${PROJECT_NAME} sandbox_example.cxx)

target_include_directories(${PROJECT_NAME} 
    PRIVATE
        ${AlphEngine2D_INCLUDE_DIRS}
)


target_link_libraries(${PROJECT_NAME} ${AlphEngine2D_TARGET} ${AlphEngine2D_DEPENDENCIES} sfml-window sfml-audio)

#ifndef SPRITE_EXAMPLE_HPP
#define SPRITE_EXAMPLE_HPP
#include "SFML/Graphics.hpp"
#include "physics_engine.hpp"

class SpriteExample
{

public:
    SpriteExample();
    ~SpriteExample();

    void run();

private:

    sf::RenderWindow fWindow;
    sf::Time fFrameRate;
    
    sf::Font fFpsFont;
    sf::Text fRealFrameRateText;

    PhysicsEngine fEngine;
    class LittleSprite* sprite;

    void add_poly(float posx, float posy);
    void add_circle(float posx, float posy);
    void add_aabb(float posx, float posy);

    void createRenderWindowPhysicalBounds();

    void update();
    void handleEvent();
    void render();

    void keyEvents(const sf::Event& event);

};

#endif //SPRITE_EXAMPLE_HPP
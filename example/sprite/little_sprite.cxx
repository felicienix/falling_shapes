#include "little_sprite.hpp"
#include "physics_engine.hpp"
#include <iostream>

LittleSprite::LittleSprite()
    :PhysicalAABB(), speed(100.f), sprite()
{
    if(!spriteTexture.loadFromFile("resources/textures/little_sprite.png"))
        throw std::runtime_error("couldn't open little_sprite.png (maybe it doesn't exist)");

    sprite.setTexture(spriteTexture, true);

    sf::FloatRect spriteBounds = sprite.getLocalBounds();
    setWidthHeight(spriteBounds.width, spriteBounds.height);
    setMass(1.f);
    fGravitySensitive = true;
    setPhysicalObjectMovementType(PhysicalObjectMovementType::Unmovable);
    fRestitutionCoeff = 0.5f;
}

LittleSprite::~LittleSprite()
{
}

void LittleSprite::handleEvent(const sf::Event &e)
{
    switch (e.type)
    {
    case sf::Event::KeyReleased:
        switch (e.key.code)
        {
        case sf::Keyboard::Left:
            fLinearVelocity -= speed * sf::Vector2f(-1.f, 0.f);
            break;
        case sf::Keyboard::Right:
            fLinearVelocity -= speed * sf::Vector2f(1.f, 0.f);
            break;
        default:
            break;
        }
        break;
    
    default:
        break;
    }
}

void LittleSprite::update(sf::Time dt)
{
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && abs(getPosition().y - yPositionBefore) <= 1e-2)
        fLinearVelocity.y = -4.f * speed;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        fLinearVelocity.x = -speed;
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        fLinearVelocity.x = speed;
    
    yPositionBefore = getPosition().y;
}

void LittleSprite::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(sprite, states);
}

#include "sprite_example.hpp"
#include "polygon.hpp"
#include "physical_circle.hpp"
#include "physical_aabb.hpp"
#include "little_sprite.hpp"
#include <numeric>
#include <iostream>
#include <random>

#define WIDTH 1500
#define HEIGHT 900


namespace
{
	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();


	static float randomFloat(float min, float max)
	{
		std::uniform_real_distribution<> distr(min, max);
		return distr(RandomEngine);
	}
}

SpriteExample::SpriteExample()
    :fWindow(sf::VideoMode(WIDTH, HEIGHT), "Bouncing Shapes", sf::Style::Close),
    fFrameRate(sf::seconds(1.f/60.f)), fRealFrameRateText(), fEngine(6), sprite(nullptr),fFpsFont()
{
    if(!fFpsFont.loadFromFile("resources/fonts/calibri.ttf"))
        throw std::runtime_error("couldn't load resources/fonts/calibri.ttf (maybe it doesn't exist)");

    fRealFrameRateText.setFont(fFpsFont);
    fRealFrameRateText.setCharacterSize(10U);
    fRealFrameRateText.setPosition(5.f, 5.f);
    fRealFrameRateText.setFillColor(sf::Color::Black);

    createRenderWindowPhysicalBounds();

    sprite = new LittleSprite();
    sprite->setPosition(WIDTH * 0.5f, HEIGHT * 0.5f);
    fEngine.addObject(sprite);
}

void SpriteExample::createRenderWindowPhysicalBounds()
{
    float half_w = 25.f;
    //floor
    PhysicalAABB* floor_poly = new PhysicalAABB(WIDTH, 2.f * half_w);
    floor_poly->setPosition(WIDTH * 0.5f, HEIGHT + half_w);
    floor_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    floor_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(floor_poly);

    //ceiling
    PhysicalAABB* ceiling_poly = new PhysicalAABB(WIDTH, 2.f * half_w);
    ceiling_poly->setPosition(WIDTH * 0.5f, -half_w);
    ceiling_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    ceiling_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(ceiling_poly);

    //wall on the right side
    PhysicalAABB* right_poly = new PhysicalAABB(2.f * half_w, HEIGHT);
    right_poly->setPosition(WIDTH + half_w, HEIGHT * 0.5f);
    right_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    right_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(right_poly);

    //wall on the left side
    PhysicalAABB* left_poly = new PhysicalAABB(2.f * half_w, HEIGHT);
    left_poly->setPosition(-half_w, HEIGHT * 0.5f);
    left_poly->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    left_poly->fRestitutionCoeff = 1.f;
    
    fEngine.addObject(left_poly);
}

SpriteExample::~SpriteExample()
{
}

void SpriteExample::run()
{
    sf::Clock appClock;
    sf::Time accu = sf::Time::Zero, deltaTime = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;

    while(fWindow.isOpen())
    {
        deltaTime = appClock.restart();
        accu += deltaTime;
        realFrameRateAccu += deltaTime;
        //update the application
        while(accu >= fFrameRate)
        {
            accu -= fFrameRate;

            handleEvent();
            update();

        } 

        //update the real frame rate displaying text
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fRealFrameRateText.setString(std::to_string((int)(1 / deltaTime.asSeconds())) + " fps\n" + std::to_string(deltaTime.asMicroseconds()) + "us");
        }
        
        render();
    }
}

void SpriteExample::keyEvents(const sf::Event& event)
{
    sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
    switch (event.key.code)
    {
    case sf::Keyboard::Escape:
        fWindow.close();
        break;
    break;
    case sf::Keyboard::P:
        add_poly(mouse_pos.x, mouse_pos.y);
    break;
    case sf::Keyboard::C:
        add_circle(mouse_pos.x, mouse_pos.y);
    break;
    case sf::Keyboard::A:
        add_aabb(mouse_pos.x, mouse_pos.y);
    break;
    default:
        break;
    }

}

void SpriteExample::handleEvent()
{
    sf::Event event;
    while(fWindow.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            fWindow.close();
            break;
        case sf::Event::KeyPressed:
            keyEvents(event);
            break;
        default:
            break;
        }

        sprite->handleEvent(event);
    }
}


void SpriteExample::add_poly(float posx, float posy)
{
    Polygon* poly = new Polygon(40.f,  6, 0.5f);
    poly->setPosition(posx, posy);
    poly->computeCentroid();
    poly->setMass(2.f);
    poly->fRestitutionCoeff = .5f;
    poly->fInitialRestCoeff = poly->fRestitutionCoeff;
    poly->fGravitySensitive = true;
    poly->rotate(45.f);

    fEngine.addObject(poly);
}

void SpriteExample::add_circle(float posx, float posy)
{
    PhysicalCircle* circle = new PhysicalCircle(40.f);
    circle->setPosition(posx, posy);
    circle->setMass(.5f);
    circle->fRestitutionCoeff = .5f;
    circle->fInitialRestCoeff = circle->fRestitutionCoeff;
    circle->fGravitySensitive = true;

    fEngine.addObject(circle);   
}

void SpriteExample::add_aabb(float posx, float posy)
{
    PhysicalAABB* aabb = new PhysicalAABB(randomFloat(10.f, 100.f), randomFloat(10.f, 100.f));
    aabb->setPosition(posx, posy);
    aabb->setMass(1.f);
    aabb->fRestitutionCoeff = 1.1f;
    aabb->fInitialRestCoeff = aabb->fRestitutionCoeff;
    aabb->fGravitySensitive = true;

    fEngine.addObject(aabb);   
}

void SpriteExample::update()
{
    sprite->update(fFrameRate);
    fEngine.update(fFrameRate);
}

void SpriteExample::render()
{
    fWindow.clear(sf::Color::White);

    for(auto physicalShape : fEngine)
    {
        fWindow.draw(*(physicalShape->getVisualMesh().get()));
    }

    fWindow.draw(*sprite);

    fWindow.draw(fRealFrameRateText);
    fWindow.display();
}


int main(int argc, char** argv)
{
    try
    {
        SpriteExample appli;

        appli.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    return 0;
}
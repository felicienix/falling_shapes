#ifndef LITTLE_SPRITE_HPP
#define LITTLE_SPRITE_HPP
#include "physical_aabb.hpp"
#include "SFML/Graphics.hpp"


class LittleSprite : public PhysicalAABB, public sf::Drawable
{
public:
    LittleSprite();
    ~LittleSprite() override;

    void handleEvent(const sf::Event& e);
    void update(sf::Time dt);

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::Texture spriteTexture;
    sf::Sprite sprite;
    float speed;

    float yPositionBefore = 0.f;

};



#endif //LITTLE_SPRITE_HPP
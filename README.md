# Falling shapes

This project aims at providing a simple 2D physics engine handling circles and convex polygons
for [SFML](https://www.sfml-dev.org/) C++ game projects.
The project has two parts, the physics engine and an example.

## The engine

The engine uses the SFML [Transformable](https://www.sfml-dev.org/documentation/2.6.0/classsf_1_1Transformable.php) object
to handle the physical object's position and rotation.

It has been implemented from scratch with the help of
two lectures from Ultrecht University,
the first one on [collision detection](https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%206%20Collision%20Detection.pdf)
and the second one on [collision resolution](https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf).

The implementation is fully open-source (see [documentation](#documentation) for more details) and you can install the library
using cmake (see below).

## Installing AlphEngine2D

To install the engine's library, you first have to have [SFML library](https://www.sfml-dev.org/download/sfml/2.6.0/) 
installed somewhere on your computer.
That is, you have to have a folder named 'SFML-versionnumber',
with 'versionnumber' being the version of SFML you downloaded (needs to be at least 2.5),
that contains the folders `include` and `lib`.
You also have to have [CMAKE](https://cmake.org/) installed and working on your device.

Once you located that folder, 
replace the path `${CMAKE_CURRENT_SOURCE_DIR}/../SFML-2.5.1` 
on line 11 of the file [CMakeLists.txt](AlphEngine2D/CMakeLists.txt) 
by the path to your SFML folder that you located earlier.
for  instance, if your path is `path/to/SFML-2.6.0`, then
the modified line would end up like this:
```
set(SFML_PATH path/to/SFML-2.6.0)
```

**WINDOWS** : if you're on windows, be careful to replace the `\` in the path by either forward slashes `/`
or doubling up the backslashes `\\`.


Then, create a folder named `build` inside the AlphEngine2D folder,
open a terminal in that folder and type :
```bash
cmake ..
```
to generate the config files and then type
```bash
cmake --build . --config Release
cmake --install .
```
to build and install the library locally.

## Examples

The first example provided is a simple sandbox-like application using the engine,
you can press 'C' to spawn a circle, 'P' to spawn a hexagon and 'A' to spawn a randomly shaped axis-aligned bounding-box (aabb) polygon.
You can also grab a shape by clicking on it and press 'space' to clone the shape
the cursor is holding.

The second example provided is showing how the engine can be used to make 
your own class interact with the physical objects of the engine.
It's a simple sprite that you can move around with the arrows
and you can also spawn circles, hexagons or aabbs by pressing 'C', 'P' or 'A' respectively
(it spawns where your cursor is).

### Running an example

A cmake file is provided that also serves as an example of how 
to link the library to your own SFML program.
With the example is provided a SFML folder for linux systems.
So if your not on linux, you'll need to do the same modification as before in the 
`CMakeLists.txt`(line 4) file of each example to have the path of the SFML folder you've installed
instead of the one provided with this project.
To run the example,
you first have to [install the engine's library](#installing-alphengine2d),
then simply go to the `example` folder of your choice, for instance the [sandbox](example/sandbox/)
folder,
create a folder named `build`, go in the `build` folder, open a
command terminal in the `build` folder and first type
```bash
cmake ..
#or type `cmake -G "MinGW Makefiles" ..` if you're c++ compilation chain is not visual studio but mingw
```
and then type
```bash
cmake --build . --config Release
```
It should generate a binary executable of the chosen example program in the `example/sandbox` folder for the sandbox example and in the `example/sprite` folder for the sprite example.

![](images/example_alphengine2d.png)

## Documentation 

To see the documentation, open the file [AlphEngine2D/doc/html/index.html](AlphEngine2D/doc/html/index.html)
with your favorite browser.



#ifndef PHYSICAL_AABB_HPP
#define PHYSICAL_AABB_HPP
#include "physical_object.hpp"

class PhysicalAABB : public PhysicalObject
{
public:
    /**
     * @brief construct a AABB rectangle of size 0x0
     * 
     */
    PhysicalAABB();
    
    /**
     * @brief construct a AABB rectangle of size _width x _height
     * 
     */
    PhysicalAABB(float _width, float _height);
    
    virtual ~PhysicalAABB() override;
    
    /**
     * @brief implementation of PhysicalObject::getBoundingRectangle but here, the bounding rectangle is exactly the AABB shape
     * 
     * @return sf::FloatRect the AABB shape in global coordinates
     */
    sf::FloatRect getBoundingRectangle() const;

    void setWidthHeight(float new_width, float new_height);
    //new_dimensions.x is the width and new_dimensions.y is the height
    void setWidthHeight(const sf::Vector2f& new_dimensions);

    //the 'X' member is the width and the 'Y' member the height
    sf::Vector2f getWidthHeight() const;

    /**
     * @brief implementation of PhysicalObject::doesCollide
     * 
     * @param other
     * @param overlap_data the resulting overlap data (if they collide) is stored in this variable 
     */
    bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) override;
    
    /** 
     * @brief implementation of PhysicalObject::computeInertia
     */
    void computeInertia() override;

    /**
     * @brief implementation of PhysicalObject::computeSweptBounds, here, to preserve the simplicity of AABB, it's just the shape moved by movedOffset
     * 
     * @param movedOffset the amount of movement
     */
    void computeSweptBounds(sf::Vector2f movedOffset) override;
    sf::Vector2f getSweptPosition() const;
    
    PhysicalObjectType getObjectType() const;

    /**
     * @brief implementation of PhysicalObject::containsPoint
     */
    bool containsPoint(const sf::Vector2f& point);

    PhysicalObject* clonePhysicalObject() const override;

    /**
     * @brief implementation of PhysicalObject::getVisualMesh
     *  
     */
    std::unique_ptr<sf::Drawable> getVisualMesh() const override;
    
    /**
	 * @brief 
	 * 	returns whether a circle and an AABB rectangle are colliding and 
	 * 	sets the overlap amount, normal vector and contact point in overlap_data.
	 * @note the normal vector direction could be circle -> aabb or aabb -> circle, so don't assume the direction is fixed.
	 */
    static bool doCircleAndAABBCollide(const sf::Vector2f& circleCenter, float circleRadius, const  sf::FloatRect& aabbRectangle, OverlapData& data);

    //non static version of the above function
    bool doesCollideWithCircle(class PhysicalCircle& other, OverlapData& data);

    /**
	 * @brief 
	 * 	returns whether two AABB rectangles are colliding and 
	 * 	sets the overlap amount and normal vector as well in overlap_data.
	 * @note doesn't compute the contact points
	 * @note the normal vector direction could be aabb1 -> aabb2 or aabb2 -> aabb1, so don't assume the direction is fixed.
	 */
    static bool doAABBAndAABBCollide(const sf::FloatRect& aabbRectangle1, const  sf::FloatRect& aabbRectangle2, OverlapData& data);
private:

    //non static version of the above function
    bool doesCollideWithAABB(PhysicalAABB& other, OverlapData& data);

    sf::Vector2f sweptPosition;
};


#endif //PHYSICAL_AABB_HPP
/**
 * @file maths_operations.hpp
 * @author felicien fiscus (felicien.fgay@gmail.com)
 * @brief utility file for the maths operations that are used by the physics engine 
 * @version 1.0.0
 * @date 2023-11-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef MATHS_OPERATIONS_HPP
#define MATHS_OPERATIONS_HPP
#include <math.h>
#include <vector>
#include "SFML/System/Vector2.hpp"

#define CROSS_PLATFORM_FLT_EPSILON 1e-10f


class MathOperations {
public:

	/**
	 * @brief computes the square of a number
	 * 
	 * @param x 
	 * @return x * x
	 */
	static float square(float x) { return x * x; }

	/**
	 * @brief equality for floating point numbers
	 * 
	 */
	static bool equalFloat(float x, float y, float eps = CROSS_PLATFORM_FLT_EPSILON)
	{
		return abs(x - y) <= eps;
	}

	/**
	 * @brief rotates the 2D vector in the trigonometric direction around the Z axis
	 * 
	 * @param vec_to_rotate 
	 * @param angle in radians
	 */
    static void rotateVector(sf::Vector2f& vec_to_rotate, float angle)
    {
        /*
         * matrix: cos -sin * x
         *         sin  cos   y
         * 
         */
        float x = vec_to_rotate.x;
        float y = vec_to_rotate.y;
        vec_to_rotate.x = std::cos(angle) * x - std::sin(angle) * y;
        vec_to_rotate.y = std::sin(angle) * x + std::cos(angle) * y;
    }

	/**
	 * @brief projects an array of points onto an axis and computes the minimum and maximum projection value
	 * 
	 * @param points 
	 * @param axis 
	 * @param min_proj output variable for the minimum projection value
	 * @param max_proj output variable for the maximum projection value
	 */
	static void projectPointsOnAxis(const std::vector<sf::Vector2f>& points,
		const sf::Vector2f& axis,
		float& min_proj, float& max_proj)
	{
		float current_proj = 0.f;
		min_proj = INFINITY, max_proj = -INFINITY;
		
		for(auto& point : points)
		{
			current_proj = MathOperations::dotProd(point, axis);
			min_proj = std::min(min_proj, current_proj);
			max_proj = std::max(max_proj, current_proj);
		}

	}

	/**
	 * @brief same as MathOperations::rotateVector but leaves the given vector untouched
	 * 
	 * @param vec_to_rotate 
	 * @param angle (in radians)
	 * @return the rotated 2D vector
	 */
	static sf::Vector2f getRotateVector(const sf::Vector2f& vec_to_rotate, float angle)
	{
        return sf::Vector2f(std::cos(angle) * vec_to_rotate.x - std::sin(angle) * vec_to_rotate.y,
		 std::sin(angle) * vec_to_rotate.x + std::cos(angle) * vec_to_rotate.y);
	}

	static sf::Vector2f normalize(const sf::Vector2f& v)
	{
		return v / (float)length(v);
	}

	/**
	 * @brief returns a.b
	 */
	static float dotProd(const sf::Vector2f& a, const sf::Vector2f& b)
	{
		return a.x * b.x + a.y * b.y;
	}

	/**
	 * @brief 
	 * 	returns v x a
	 */
	static sf::Vector2f Cross2DScalar( const sf::Vector2f& v, float a )
	{
		return sf::Vector2f(a * v.y, -a * v.x);
	}

	/**
	 * @brief 
	 * 	returns a x v
	 */
	static sf::Vector2f Cross2DScalar(float a, const sf::Vector2f& v )
	{
		return sf::Vector2f(-a * v.y, a * v.x);
	}

	/**
	 * @brief 
	 * 	returns the value of v1 x v2 along the Z axis
	 */
	static float crossProduct2D(const sf::Vector2f& v1, const sf::Vector2f& v2)
	{
		/*
		 * reminder:  a x b = (a1.u_x + a2.u_y) x (b1.u_x + b2.u_y)
		 * 					= a1b2 - a2b1 .u_z
		 * 
		 */
		return v1.x * v2.y - v1.y * v2.x;
	}

	/**
	 * @brief 
	 * 	returns the area of a triangle with sides of length a, b and c respectively.
	 */
	static float triangleArea(float a, float b, float c)
	{
		float s = a + b + c;
		s *= 0.5f;

		return std::sqrt(s * (s - a) * (s - b) * (s - c));
	}

	/**
	 * @brief 
	 * 	returns the eucldidian norm of vec
	 */
	static float length(const sf::Vector2f& vec)
	{
		return std::sqrt(square(vec.x) + square(vec.y));
	}

	/**
	 * @brief 
	 * 	returns euclidian norm of b - a
	 */
	static float getDistance(const sf::Vector2f& a, const sf::Vector2f& b)
	{
		return length(b - a);
	}
};

#endif //MATHS_OPERATIONS_HPP
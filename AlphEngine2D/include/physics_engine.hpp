/**
 * @file physics_engine.hpp
 * @author felicien fiscus (felicien.fgay@gmail.com)
 * @brief the actual physics engine that runs the simulation loop and computes the physical impulses for each collision
 * @version 1.0.0
 * @date 2023-11-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef PHYSICS_ENGINE_HPP
#define PHYSICS_ENGINE_HPP
#include "physical_object.hpp"
#include <list>

/**
 * @brief simply two physical objects that are colliding with their associated collision data
 * 
 */
struct CollisionPair
{
    CollisionPair(PhysicalObject* _a, PhysicalObject* _b, OverlapData d)
        :a(_a), b(_b), data(d){}
    PhysicalObject* a;
    PhysicalObject* b;
    OverlapData data;
};

class PhysicsEngine
{
public:
    /**
     * @brief the physics engine runs a simulating loop which computes collisions, resolve the velocities and
     * apply the velocities. 
     * 
     * @param sub_steps Those three steps sometimes need to be done multiple times during one loop iteration,
     * hence the sub_steps parameter which corresponds to the amount of times the engine does these steps in a single loop iteration.
     *  
     */
    PhysicsEngine(unsigned int sub_steps = 8);
    ~PhysicsEngine();

    /**
     * @brief adds a new object to the physics engine, making it part of the physics simulation and interacting with the other objects
     * that are in the engine.
     * 
     * @param obj 
     */
    void addObject(PhysicalObject* obj);
    /**
     * @brief 
     *  remove physical object from simulation, it will no longer interact with the other objects 
     */
    void removeObject(PhysicalObject* obj);

    void removeGravityOnObject(PhysicalObject* obj);
    
    /**
     * @brief Set the conversion factor which is the number of meters one pixel is equivalent to (i.e., the scale of the simulation).
     * 
     * @param newConversionFactor the new conversion factor value, for instance the value 1e-3f would mean having 1 pixel be equivalent to 
     * 1 millimiter in the physics simulation.
     */
    void setPixelToMetersConversionFactor(float newConversionFactor);
    /**
     * @brief Get the Pixel To Meters Conversion Factor object
     * 
     * @see setPixelToMetersConversionFactor
     * @return float 
     */
    float getPixelToMetersConversionFactor() const;
    float getInversePixelToMetersConversionFactor() const;

    /**
     * @brief 
     *  renders 'obj' sensitive to gravity if it wasn't before (see removeGravityOnObject),
     * does nothing otherwise.
     */
    void addGravityOnObject(PhysicalObject* obj);

    float fGravityConstant = 9.81f;
    
    /**
     * @brief simulation loop, where the engine computes collisions and resolve them.
     * 
     * @param dt the amount of time since the last call to this function was made
     */
    void update(sf::Time dt);

    /**
     * @brief 
     *  returns the iterator of the first physical object 
     */
    std::vector<PhysicalObject*>::iterator begin();
    std::vector<PhysicalObject*>::const_iterator begin() const;
    /**
     * @brief 
     *  returns an iterator that points to one past the last physical object 
     */
    std::vector<PhysicalObject*>::iterator end();
    std::vector<PhysicalObject*>::const_iterator end() const;

    /**
     * @brief 
     *  returns a reference to the list of all the physical objects sensitive to gravity 
     */
    std::list<PhysicalObject*>& getGravitySensitiveObjects();

    /**
     * @brief 
     *  returns a reference to the list containing every physical object of movementn type 'type'
     */
    std::list<PhysicalObject*>& getPhysicalObjectsByMovementType(PhysicalObjectMovementType::Type type);

private:
    std::vector<PhysicalObject*> fObjects;
    std::array<std::list<PhysicalObject*>, (size_t)PhysicalObjectMovementType::MovementTypeCount> fObjectsByType;
    std::list<PhysicalObject*> fGravitySensitiveObjects;
    std::list<CollisionPair> fCollisionPairs;
    unsigned int fSubsteps = 8;
    float CONVERSION_FACTOR = 1e-3f * 5.f;
    float INVERSE_CONVERSION_FACTOR = 1e3f * 0.2f; //1px -> 5mm which means, 1meter == 1e3f * 0.2 pixels

    void integrateForces(sf::Time dt);
    void applyGravity();
    void computeSweptBounds(sf::Time dt);
    void clearForces();
    void updatePositions(sf::Time dt);
    void updateVelocitiesWithImpulses(sf::Time dt);
    void computeCollisions();

};

#endif //PHYSICS_ENGINE_HPP
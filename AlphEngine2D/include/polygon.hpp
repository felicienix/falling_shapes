/**
 * @file polygon.hpp
 * @author felicien fiscus (felicien.fgay@gmail.com)
 * @brief A convex polygon that can collide and interact with other physical objects.
 * A Polygon is static, that is once the points are set, you can't move them around to modify the polygon's shape.
 * Also, the position of a polygon is defined by the position of its centroid in space.
 * @version 1.0.0
 * @date 2023-11-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "physical_object.hpp"


class Polygon : public PhysicalObject
{
public:

    /**
     * @brief A polygon with 0 points
     */
    Polygon();
    /** 
     * @brief creates a regular polygon with a certain number of points (3 -> triangle, 4 -> square, 5 -> pentagon, ...) 
     */
    Polygon(float circumscribed_circle_radius, unsigned int number_of_points = 3, float mass = 0.f);
    Polygon(const Polygon& poly);
    
    virtual ~Polygon() override;

    /**
     * @brief sets the points that makes the polygon, it can be used to create a custom polygon.
     * 
     * @note the polygon needs to be convex for the engine to work. This function doesn't check the convexity.
     * @param points needs to include every point only once ! 
     *  [p1, ..., pn] (pn != p1) --> OK
     *  [p1, ..., pn, p1] --> NOT OK
     */
    void setPolygon(const std::vector<sf::Vector2f>& points);

    /**
     * @brief 
     *  returns the length of the first side of the polygon (in the order provided by the array of points)
     * @note only really useful for regular polygons
     */
    float getSideLength() const;

    std::unique_ptr<sf::Drawable> getVisualMesh() const override;

    /**
     * @brief
	 * 	overrides the PhysicalObject function for the case of collisions including a polygon
     */
    bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) override;

    /**
     * @brief polygon implementation of PhysicalObject::computeSweptBounds
     * 
     * @note because the AABB collision is supposed to stay simple, this function does nothing
     * 
     */
    void computeSweptBounds(sf::Vector2f movedOffset) override;

    /**
     * @brief polygon implementation of PhysicalObject::containsPoint
     * 
     * @note can take up to O(number of points) to compute 
     * 
     */
    bool containsPoint(const sf::Vector2f& point) override;

    /**
     * @brief polygon implementation of PhysicalObject::computeInertia 
     * 
     */
    void computeInertia() override;

    PhysicalObjectType getObjectType() const override;

    PhysicalObject* clonePhysicalObject() const override;

	float computeSignedArea();

    void computeCentroid();

    /**
     * @brief returns the points making up the polygon in local coordinates, meaning (0,0) is the centroid point,
     *  and without taking the rotation into account
     */
    const std::vector<sf::Vector2f>& getPointsWithoutTranform() const;
    /**
     * @brief returns the points making up the polygon in global coordinates with the rotation taken into account
     *   
     */
    std::vector<sf::Vector2f> transformedPoints();
    /**
     * @brief returns the points making up the swept bounds (computed by the last call to computeSweptBounds)
     *  of the polygon in global coordinates with the rotation taken into account
     *   
     */
    std::vector<sf::Vector2f> transformedSweptBounds();

    sf::Vector2f getSweptPosition() const;

private:
    std::vector<sf::Vector2f> fSweptBounds;
    sf::Vector2f sweptPosition;

    std::vector<sf::Vector2f> fPoints; 
    //used by the containsPoint method
    float farthestDistanceFromCentroid; //the distance between the farthest point from the centroid and the centroid

    float computeTriangleInertia(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3);

    //backend function to test collision and compute contact point
    //with another polygon using SAT algorithm
    bool doesCollideWithPoly(Polygon& other, OverlapData& overlap_data);
    bool doesCollideWithAABB(class PhysicalAABB& other, OverlapData& overlap_data);
public:
    static void computeContactPoints(const std::vector<sf::Vector2f>& points, const std::vector<sf::Vector2f>& other_points, OverlapData& overlap_data);
private:
    static struct FeatureEdge bestFeature(const sf::Vector2f& normal, 
        const std::vector<sf::Vector2f>& points, bool other);
public:

    /**
     * @brief 
     *  uses the SAT algorithm to test whether two polygons collide, and sets the collision normal, collision overlap amount.
     * @note the poly1 and poly2 needs to be arrays like [p1, ..., pn, p1] where the last point is equal to the first point /!\
     * @note it doesn't compute the contact points, use computeContactPoints method for that.
     * @note the normal could point in different directions depending on which polygon axes it collides with,
     */
    static bool doPolygonsCollide(const std::vector<sf::Vector2f>& poly1, const std::vector<sf::Vector2f>& poly2, OverlapData& collision_data);

    /**
     * @brief computes the signed area of a polygon using the specified array of points
     * 
     * @param points needs to be of the format: [p1, ..., pn] with pn != p1
     * 
     * @return float the signed area of the specified polygon
     */
    static float computeSignedArea(const std::vector<sf::Vector2f>& points)
    {
        float area = 0.f;
        for(unsigned int i = 0;i < points.size();++i)
        {
            area += points[i].x * points[(i+1)%points.size()].y
                - points[(i+1)%points.size()].x * points[i].y;
        }
        area *= 0.5f;

        return area;
    }
    /**
     * @brief computes the centroid point of a polygon using the specified array of points
     * 
     * @param points needs to be of the format: [p1, ..., pn] with pn != p1
     * 
     * @return sf::Vector2f the centroid point of the specified polygon
     */
    static sf::Vector2f computeCentroid(const std::vector<sf::Vector2f>& points)
    {
        sf::Vector2f centroid(0.f,0.f);
        float area = computeSignedArea(points);
        if(std::abs(area) <= CROSS_PLATFORM_FLT_EPSILON) return centroid;
        float product_diff = 0.f;

        for(unsigned int i = 0;i<points.size();++i)
        {
            product_diff = points[i].x * points[(i+1)%points.size()].y 
                - points[(i+1)%points.size()].x * points[i].y;
            centroid.x += (points[i].x + points[(i+1)%points.size()].x) * product_diff;
            centroid.y += (points[i].y + points[(i+1)%points.size()].y) * product_diff;
        }
        centroid /= (6.f * area);

        return centroid;
    }
};

#endif //POLYGON_HPP
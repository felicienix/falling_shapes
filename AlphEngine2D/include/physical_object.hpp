/**
 * @file physical_object.hpp
 * @author felicien fiscus (felicien.fgay@gmail.com)
 * @brief a physical object is an object that can collide with other physical objects,
 *  has an inertia and has bounds. It is the abstract object used by the physics engine.
 * @version 1.0.0
 * @date 2023-11-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef PHYSICAL_OBJECT_HPP
#define PHYSICAL_OBJECT_HPP
#include "SFML/Graphics/Transformable.hpp"
#include "SFML/Graphics/Drawable.hpp"
#include "SFML/System/Time.hpp"
#include "maths_operations.hpp"
#include <array>
#include <memory>
#define PI 3.14159265359

/**
 * @brief struct used to store the contact points of a collision (2 maximum)
 * 
 */
struct ContactPoints
{
    size_t size;
    sf::Vector2f points[2];

    ContactPoints();

    ContactPoints(const ContactPoints& other);

    sf::Vector2f& operator[](size_t index);

    void clear();

    void add(const sf::Vector2f& p);

    void remove(const sf::Vector2f& p);
};

/**
 * @brief the overlap data associated with each collision happening
 * 
 */
struct OverlapData
{
    OverlapData()
        :fCollisionNormal(), fOverlapAmount(0.f), fContactPoints()
    {}
    sf::Vector2f fCollisionNormal;
    float fOverlapAmount;
    ContactPoints fContactPoints;
};

enum class PhysicalObjectType
{
    Polygon, Circle, AABB
};

/**
 * @brief the different physical object that there is,
 * the 'static' types are physical objects that don't move at all and so are not subjected to collision resolution.
 * The 'unmovable' types are physical objects that cannot be moved laterally by colliding with other physical object
 * (other than just solving the collision, see PhysicalObject::solveCollisions) but can be moved by gravity for instance
 * (if the object is gravity sensitive) or by another physical object falling on them,
 * it also doesn't rotate.
 * Finally, the 'movable' types are physical object that will be subjected to all collision resolutions and physical impulses.
 * 
 */
namespace PhysicalObjectMovementType
{
    enum Type
    {
    Static, Unmovable, Movable, MovementTypeCount
    };
}

/**
 * @brief describes with what type object will the physical object interact
 *  OnlyStatic : only with objects of movement type Static
 *  OnlyStaticAndItsKind: only with object of movement type Static and objects with the same movement type as the object itself
 *  WithEverything : interacts with every physical objects in the engine
 */
namespace PhysicalObjectInteractionType
{
    enum Type
    {
        OnlyStatic, OnlyStaticAndItsKind, WithEverything
    };
}

class PhysicalObject : public sf::Transformable
{
protected:
    float fInertiaMomentum = 0.f;
    float fInverseInertia = std::numeric_limits<float>::max();
    float fArea = 0.f;
    sf::Vector2f fCentroid;
    sf::FloatRect fLocalBounds;

    sf::Vector2f forces;
    float torques;

    float fMass = 0.5f; //in kg
    float fInverseMass = 2.f;
    PhysicalObjectMovementType::Type fPhysicalObjectMovementType = PhysicalObjectMovementType::Movable;
public:
    PhysicalObject();
    virtual ~PhysicalObject();
 
    /**
     * @brief returns the smallest possible rectangle inside which the physical object can be
     * 
     * @note for polygons, it can sometimes be an approximation and not be the smallest rectangle,
     * but the physical object can always fit inside of it.
     * 
     */
    virtual sf::FloatRect getBoundingRectangle() const;

    /**
     * @brief returns whether this physical object and 'other' are colliding or not
     * 
     * @param other
     * @param overlap_data the resulting overlap data (if they collide) is stored in this variable 
     */
    virtual bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) = 0;
    
    /** 
     * @brief to call when the initial inertia has been modified, for example a change of mass or area.
     */
    virtual void computeInertia() = 0;

    /**
     * @brief computes the swept bounds of the physical shape when wanting to move a certain amount,
     *  this function is called before actually moving the object in order to test for collisions.
     * 
     * @param movedOffset the amount of movement
     */
    virtual void computeSweptBounds(sf::Vector2f movedOffset) = 0;
    
    virtual float getInertiaMomentum() const;
    virtual float getInverseInertia() const;

    virtual PhysicalObject* clonePhysicalObject() const = 0;

    /**
     * @brief returns a pointer to a drawable mesh of the physical object, mainly used for debug purposes
     *  
     */
    virtual std::unique_ptr<sf::Drawable> getVisualMesh() const = 0;

	virtual float getArea() const;
    float getMass() const;
    float getInverseMass() const;
    
    /**
     * @brief updates the mass of the object and the inertia momentum
     * 
     * @param new_mass in kg
     */
    void setMass(float new_mass);

    /**
     * @brief add a force to apply on the next update call
     * @note the force will only be applied once on the next update call as
     *  the forces and torques are cleared up at the end of every update call
     *
     * @param force the force vector to apply (in N)
     */
    void applyForce(const sf::Vector2f& force);
    /**
     * @brief add a torque to apply on the next update call
     * @note the torque will only be applied once on the next update call as
     *  the forces and torques are cleared up at the end of every update call
     * 
     * @param torque the torque (in N.m) to apply
     */
    void applyTorque(float torque);

    /**
     * @brief resets forces and torques to be zero
     */
    void clearForces();

    /**
     * @brief modifies the linear and angular velocities to take the forces / torques in account
     * using newton's law : acceleration = inverse_mass * sum of all forces
     * and velocity = acceleration * dt
     */
    void intergrateForcesAndTorque(sf::Time dt);

    virtual sf::Vector2f getCentroid() const;

    virtual PhysicalObjectType getObjectType() const = 0;
    
    PhysicalObjectMovementType::Type getObjectMovementType() const;
    /**
     * @brief modifies the movement type of the physical object,
     *  For the STATIC type, it sets the mass of the object to be infinite and disables gravity.
     * @see PhysicalObjectMovementType
     * 
     * @param new_type 
     */
    void setPhysicalObjectMovementType(PhysicalObjectMovementType::Type new_type);

    /**
     * @brief solves collision by separating the objects 'this' and 'other' that are currently colliding
     * 
     * @param other 
     * @param overlap_data the data associated with the collision with 'other'
     */
    void solveCollisions(PhysicalObject& other, OverlapData overlap_data);

    /**
     * @brief 
     *  returns whether a point is inside or outside of a physical object's shape
     */
    virtual bool containsPoint(const sf::Vector2f& point) = 0;

    sf::Vector2f fLinearVelocity;
    float fAngularVelocity = 0.f;
    float fRestitutionCoeff = 0.f;
    float fInitialRestCoeff = 1.f;
    bool fGravitySensitive = false;
    float fStaticFriction = 1.f;
    float fDynamicFriction = 0.48f;
    PhysicalObjectInteractionType::Type fInteractionType = PhysicalObjectInteractionType::WithEverything;
};

#endif //PHYSICAL_OBJECT_HPP
/**
 * @file physical_circle.hpp
 * @author felicien fiscus (felicien.fgay@gmail.com)
 * @brief A circle that can collide and interact with other physical objects.
 * @version 1.0.0
 * @date 2023-11-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef PHYSICAL_CIRCLE_HPP
#define PHYSICAL_CIRCLE_HPP
#include "physical_object.hpp"

class PhysicalCircle : public PhysicalObject
{
public:
    PhysicalCircle(float _radius = 0.f);
    
    /**
     * @brief updates the radius as well as the interia momentum of the circle
     * 
     * @param new_radius 
     */
    void setRadius(float new_radius);

    float getRadius() const;
    
	/**
	 * @brief 
	 * 	overrides the PhysicalObject function for the case of collisions including a circle
	 */
    bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) override;

	/**
	 * @brief 
	 * 	returns whether a circle is colliding with a polygon 'poly' and 
	 * 	sets the overlap amount and normal vector as well in overlap_data.
	 * @note doesn't compute the contact points
	 * @note the normal vector direction could be poly -> circle or circle->poly, so don't assume the direction is fixed.
	 */
    static bool doCircleCollideWithPoly(const std::vector<sf::Vector2f>& poly, const sf::Vector2f& circleCenter, float circleRadius, OverlapData& overlap_data);
	/**
	 * @brief 
	 * 	returns whether two circles are colliding and 
	 * 	sets the overlap amount and normal vector as well in overlap_data.
	 * @note doesn't compute the contact points
	 * @note the normal vector direction could be circle 1 -> circle 2 or circle 2 -> circle 1, so don't assume the direction is fixed.
	 */
    static bool doCircleCollideWithCircle(const sf::Vector2f& circleCenter1, float circleRadius1,
							 const sf::Vector2f& circleCenter2, float circleRadius2, OverlapData& overlap_data);

	//non static versions of the above
    bool doesCollideWithPoly(class Polygon& other, OverlapData& overlap_data);
    bool doesCollideWithCircle(PhysicalCircle& other, OverlapData& overlap_data);

	

    PhysicalObject* clonePhysicalObject() const override;

	/**
     * @brief circle implementation of PhysicalObject::computeInertia 
	 */
    void computeInertia() override;
	/**
	 * @brief 
	 * 	for now it does nothing except having a fake circle move by movedOffset because I haven't implemented collision detection with a stadium shape
	 */
    void computeSweptBounds(sf::Vector2f movedOffset) override;
	sf::Vector2f getSweptPosition() const;

	/**
	 * @brief 
	 * 	returns the point in 'points' that is the closest to a circle 
	 */
    static sf::Vector2f closestPointToCircle(const sf::Vector2f& circleCenter, float circleRadius, const std::vector<sf::Vector2f>& points);

    PhysicalObjectType getObjectType() const override;

    sf::FloatRect getBoundingRectangle() const override;

	/**
     * @brief circle implementation of PhysicalObject::containsPoint 
	 * 
	 */
    bool containsPoint(const sf::Vector2f& point) override;

    std::unique_ptr<sf::Drawable> getVisualMesh() const override;

private:

    float radius;
    static void projectCircle(const sf::Vector2f& circleCenter, float circleRadius, float& min_proj, float& max_proj, const sf::Vector2f& proj_axis);

	sf::Vector2f sweptPosition;
};

#endif //PHYSICAL_CIRCLE_HPP
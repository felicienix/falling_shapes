var searchData=
[
  ['setmass_0',['setMass',['../classPhysicalObject.html#a64c19a8329204a4ef44012c4cf70379d',1,'PhysicalObject']]],
  ['setphysicalobjectmovementtype_1',['setPhysicalObjectMovementType',['../classPhysicalObject.html#a9da2b7890db483797eb7e7161805b379',1,'PhysicalObject']]],
  ['setpixeltometersconversionfactor_2',['setPixelToMetersConversionFactor',['../classPhysicsEngine.html#a4f97e73d6c227a1a4effa46f4f8c93bf',1,'PhysicsEngine']]],
  ['setpolygon_3',['setPolygon',['../classPolygon.html#a67611688430ed68d025b4895edc7ad57',1,'Polygon']]],
  ['setradius_4',['setRadius',['../classPhysicalCircle.html#a4213883d2f0191786c1ff333aac55621',1,'PhysicalCircle']]],
  ['solvecollisions_5',['solveCollisions',['../classPhysicalObject.html#a825f13f13c4698a3c87c9ccc5da9a194',1,'PhysicalObject']]],
  ['square_6',['square',['../classMathOperations.html#a516abff335f25bdcf6a417ac256cecef',1,'MathOperations']]]
];

var searchData=
[
  ['getboundingrectangle_0',['getboundingrectangle',['../classPhysicalAABB.html#a0a98d05b633b98206afc3881fb7af490',1,'PhysicalAABB::getBoundingRectangle()'],['../classPhysicalCircle.html#a7ce58933d583a54946531981083c9553',1,'PhysicalCircle::getBoundingRectangle()'],['../classPhysicalObject.html#af3510ab3ae5605884a377f1cdc485009',1,'PhysicalObject::getBoundingRectangle()']]],
  ['getdistance_1',['getDistance',['../classMathOperations.html#a7ceb817e4b68da09a85bf2d38065c0e7',1,'MathOperations']]],
  ['getgravitysensitiveobjects_2',['getGravitySensitiveObjects',['../classPhysicsEngine.html#a473878d444c26413af2fc16629005483',1,'PhysicsEngine']]],
  ['getphysicalobjectsbymovementtype_3',['getPhysicalObjectsByMovementType',['../classPhysicsEngine.html#a9c2966745d9270b7c91791edeee49042',1,'PhysicsEngine']]],
  ['getpixeltometersconversionfactor_4',['getPixelToMetersConversionFactor',['../classPhysicsEngine.html#a8a63a024f99a4b6e30c971210dbbdf8d',1,'PhysicsEngine']]],
  ['getpointswithouttranform_5',['getPointsWithoutTranform',['../classPolygon.html#a4d1cc4918c626904cf87470a0b0bf73c',1,'Polygon']]],
  ['getrotatevector_6',['getRotateVector',['../classMathOperations.html#a56cc8a3c3604b62ff2caeaf24fde6a2b',1,'MathOperations']]],
  ['getsidelength_7',['getSideLength',['../classPolygon.html#ade203772f68e190358314a807e13c98d',1,'Polygon']]],
  ['getvisualmesh_8',['getvisualmesh',['../classPhysicalAABB.html#a6ac8a292a705103a47a7e4b5844e80b0',1,'PhysicalAABB::getVisualMesh()'],['../classPhysicalCircle.html#abe23fc84caaa3f4d8f2ad23f01c72e6c',1,'PhysicalCircle::getVisualMesh()'],['../classPhysicalObject.html#a863a4b1d1aaf9ed8bc3291807ad8103d',1,'PhysicalObject::getVisualMesh()'],['../classPolygon.html#a50197ef6afefbd210d4a53a559a7d5f5',1,'Polygon::getVisualMesh()']]]
];

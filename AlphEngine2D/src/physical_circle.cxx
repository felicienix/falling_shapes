#include "physical_circle.hpp"
#include "polygon.hpp"
#include "physical_aabb.hpp"
#include "SFML/Graphics/CircleShape.hpp"
#include <algorithm>

PhysicalCircle::PhysicalCircle(float _radius)
    :PhysicalObject(), radius(_radius)
{
    fArea = PI * radius * radius;
    fCentroid.x = radius;
    fCentroid.y = radius;
    setOrigin(fCentroid);
    fLocalBounds.width = 2.f * radius;
    fLocalBounds.height = 2.f * radius;
    computeInertia();
}


sf::FloatRect PhysicalCircle::getBoundingRectangle() const
{
    sf::Vector2f position(getPosition() - fCentroid + sf::Vector2f(fLocalBounds.left, fLocalBounds.top));
    sf::Vector2f dimensions(fLocalBounds.width, fLocalBounds.height);

    return sf::FloatRect(position, dimensions);
}

bool PhysicalCircle::containsPoint(const sf::Vector2f& point)
{
    return MathOperations::dotProd(point - getPosition(), point - getPosition()) <= radius * radius;
}

std::unique_ptr<sf::Drawable> PhysicalCircle::getVisualMesh() const
{
    auto mesh = std::make_unique<sf::CircleShape>();
    (*mesh.get()).setRadius(radius);
    (*mesh.get()).setFillColor(sf::Color::Transparent);
    (*mesh.get()).setOutlineColor(sf::Color::Blue);
    (*mesh.get()).setOutlineThickness(1.f);
    (*mesh.get()).setOrigin(radius, radius);
    (*mesh.get()).setPosition(getPosition());

    return std::move(mesh);
}

void PhysicalCircle::setRadius(float new_radius)
{
    radius = new_radius;
    fArea = PI * radius * radius;
    computeInertia();
    fLocalBounds.width = 2.f * radius;
    fLocalBounds.height = 2.f * radius;
}

float PhysicalCircle::getRadius() const
{
    return radius;
}

bool PhysicalCircle::doesCollide(PhysicalObject& other, OverlapData& overlap_data)
{
    switch (other.getObjectType())
    {
    case PhysicalObjectType::Circle:
    {
        PhysicalCircle& circle = static_cast<PhysicalCircle&>(other);
        return doesCollideWithCircle(circle, overlap_data);
    }
        break;
    case PhysicalObjectType::Polygon:
    {
        Polygon& poly = static_cast<Polygon&>(other);
        return doesCollideWithPoly(poly, overlap_data);
    }
        break;
    case PhysicalObjectType::AABB:
    {
        PhysicalAABB& aabb = static_cast<PhysicalAABB&>(other);
        if(aabb.doesCollideWithCircle(*this, overlap_data))
        {
            overlap_data.fCollisionNormal *= -1.f;
            return true;
        }

        return false;
    }
    default:
        break;
    }

    return false;
}

bool PhysicalCircle::doCircleCollideWithPoly(const std::vector<sf::Vector2f> &poly, const sf::Vector2f &circleCenter, float circleRadius, OverlapData &overlap_data)
{
    //Implementation of SAT (Seperated Axis Theorem)
    //check the projection of every edge against every perpendicular axis
    //if there is an axis on which the projections do not intersect, then there is no collision


    auto* other_one = &poly;
    //other_one->pop_back();

    overlap_data.fOverlapAmount = INFINITY;
    float overlap = 0.f;    //overlap computed at each iteration

    float min_proj_this = 0.f, max_proj_this = 0.f;
    float min_proj_other = 0.f, max_proj_other = 0.f;
    
    for(int k = 0;k < (int)(other_one->size() - 1);++k)
    {
        //first compute the axis to project on
        sf::Vector2f axis((*other_one)[k+1] - (*other_one)[k]);
        MathOperations::rotateVector(axis, PI/2.f);
        axis /= MathOperations::length(axis);

        //then project every point on it and find min / max
        MathOperations::projectPointsOnAxis(*other_one, axis, min_proj_other, max_proj_other);

        //points of other_one
        projectCircle(circleCenter, circleRadius, min_proj_this, max_proj_this, axis);

        //minimum of maximums - maximum of minimums
        overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
        if(overlap < overlap_data.fOverlapAmount)
        {
            overlap_data.fOverlapAmount = overlap;
            overlap_data.fCollisionNormal = axis;
        }
        
        //then test if they do not intersects, if so then other and this do not collide, if not we continue testing
        if(overlap < 0.f)
            return false;
    }
    //if we arrive to this point, it can happen that the polygon is intersecting the bounding box of the circle
    //but not the circle itself, so we check for one last axis that can potentially be a separating axis

    sf::Vector2f last_axis = MathOperations::normalize(closestPointToCircle(circleCenter, circleRadius, *other_one) - circleCenter);

    MathOperations::projectPointsOnAxis(*other_one, last_axis, min_proj_other, max_proj_other);
    projectCircle(circleCenter, circleRadius, min_proj_this, max_proj_this, last_axis);

    //minimum of maximums - maximum of minimums
    overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
    //then test if they do not intersects, if so then other and this do not collide
    if(overlap < 0.f)
        return false;
    
    //if we arrive here than they collide but we need to potentially update the normal collision and collision depth
    if(overlap < overlap_data.fOverlapAmount)
    {
        overlap_data.fOverlapAmount = overlap;
        overlap_data.fCollisionNormal = last_axis;
    }

    return true;
}

void PhysicalCircle::projectCircle(const sf::Vector2f& circleCenter, float circleRadius, float& min_proj, float& max_proj, const sf::Vector2f& proj_axis)
{
    float current_proj = MathOperations::dotProd(circleCenter, proj_axis);

    min_proj = std::min(current_proj + circleRadius, current_proj - circleRadius);
    max_proj = std::max(current_proj + circleRadius, current_proj - circleRadius);
}

bool PhysicalCircle::doCircleCollideWithCircle(const sf::Vector2f &circleCenter1, float circleRadius1, const sf::Vector2f &circleCenter2, float circleRadius2, OverlapData &overlap_data)
{
    float collision_test = MathOperations::length(circleCenter2 - circleCenter1);

    if(collision_test <= circleRadius1 + circleRadius2)   //it's colliding
    {
        overlap_data.fCollisionNormal = circleCenter2 - circleCenter1;
        float normalLength = MathOperations::length(overlap_data.fCollisionNormal);
        if(normalLength <= CROSS_PLATFORM_FLT_EPSILON)
        {
            overlap_data.fCollisionNormal = sf::Vector2f(1.f, 0.f);
            normalLength = 1.f;
        } 
        overlap_data.fCollisionNormal /= normalLength;
        overlap_data.fOverlapAmount = (circleRadius1 + circleRadius2 - collision_test) * 0.5f;

        return true;
    }

    return false;
}

bool PhysicalCircle::doesCollideWithPoly(Polygon &other, OverlapData &overlap_data)
{
    //Implementation of SAT (Seperated Axis Theorem)
    //check the projection of every edge against every perpendicular axis
    //if there is an axis on which the projections do not intersect, then there is no collision


    auto points_other = other.transformedSweptBounds();
    auto* other_one = &points_other;
    //other_one->pop_back();

    overlap_data.fOverlapAmount = INFINITY;
    float overlap = 0.f;    //overlap computed at each iteration

    float min_proj_this = 0.f, max_proj_this = 0.f;
    float min_proj_other = 0.f, max_proj_other = 0.f;
    
    for(int k = 0;k < (int)(other_one->size() - 1);++k)
    {
        //first compute the axis to project on
        sf::Vector2f axis((*other_one)[k+1] - (*other_one)[k]);
        MathOperations::rotateVector(axis, PI/2.f);
        axis /= MathOperations::length(axis);

        //then project every point on it and find min / max
        MathOperations::projectPointsOnAxis(*other_one, axis, min_proj_other, max_proj_other);

        //points of other_one
        projectCircle(sweptPosition, getRadius(), min_proj_this, max_proj_this, axis);

        //minimum of maximums - maximum of minimums
        overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
        if(overlap < overlap_data.fOverlapAmount)
        {
            overlap_data.fOverlapAmount = overlap;
            overlap_data.fCollisionNormal = axis;
        }
        
        //then test if they do not intersects, if so then other and this do not collide, if not we continue testing
        if(overlap < 0.f)
            return false;
    }
    //if we arrive to this point, it can happen that the polygon is intersecting the bounding box of the circle
    //but not the circle itself, so we check for one last axis that can potentially be a separating axis

    sf::Vector2f last_axis = MathOperations::normalize(closestPointToCircle(sweptPosition, getRadius(), *other_one) - sweptPosition);

    MathOperations::projectPointsOnAxis(*other_one, last_axis, min_proj_other, max_proj_other);
    projectCircle(sweptPosition, getRadius(), min_proj_this, max_proj_this, last_axis);

    //minimum of maximums - maximum of minimums
    overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
    //then test if they do not intersects, if so then other and this do not collide
    if(overlap < 0.f || overlap >1e14f || std::isnan(overlap))
        return false;
    
    //if we arrive here than they collide but we need to potentially update the normal collision and collision depth
    if(overlap < overlap_data.fOverlapAmount)
    {
        overlap_data.fOverlapAmount = overlap;
        overlap_data.fCollisionNormal = last_axis;
    }

    overlap_data.fCollisionNormal *= 
        MathOperations::dotProd(sweptPosition - other.getSweptPosition(), overlap_data.fCollisionNormal) > 0.f ? -1.f : 1.f;
    
    solveCollisions((PhysicalObject&)other, overlap_data);

    //determining the contact point
    overlap_data.fContactPoints.clear();
    overlap_data.fContactPoints.add(sweptPosition + getRadius() * overlap_data.fCollisionNormal);

    return true;

}

bool PhysicalCircle::doesCollideWithCircle(PhysicalCircle& other, OverlapData& overlap_data)
{
    float r1 = getRadius();
    float r2 = other.getRadius();
    sf::Vector2f c1 = sweptPosition;
    sf::Vector2f c2 = other.sweptPosition;
    float collision_test = MathOperations::length(c2 - c1);

    if(collision_test <= r1 + r2)   //it's colliding
    {
        overlap_data.fCollisionNormal = c2 - c1;
        float normalLength = MathOperations::length(overlap_data.fCollisionNormal);
        if(normalLength <= CROSS_PLATFORM_FLT_EPSILON)
        {
            overlap_data.fCollisionNormal = sf::Vector2f(1.f, 0.f);
            normalLength = 1.f;
        } 
        overlap_data.fCollisionNormal /= normalLength;
        overlap_data.fOverlapAmount = (r1 + r2 - collision_test) * 0.5f;
        overlap_data.fCollisionNormal *= (MathOperations::dotProd(other.sweptPosition - sweptPosition, overlap_data.fCollisionNormal) <= 0.f) ? -1.f : 1.f; 
        solveCollisions(other, overlap_data);

        overlap_data.fContactPoints.clear();
        overlap_data.fContactPoints.add(sweptPosition + r1 * overlap_data.fCollisionNormal);

        return true;
    }

    return false;
}

PhysicalObject* PhysicalCircle::clonePhysicalObject() const
{
    return new PhysicalCircle(*this);
}

void PhysicalCircle::computeInertia()
{
    if(fMass < 0.f) //check if its mas is infinite
    {
        fInertiaMomentum = std::numeric_limits<float>::max();
        fInverseInertia = 0.f;
        fInverseMass = 0.f;
        return;
    }

    float rho = fMass / fArea;
    fInertiaMomentum = rho * PI * std::pow(getRadius(), 4) * 0.5f;
    fInverseInertia = 1.f / fInertiaMomentum;
    fInverseMass = 1.f / fMass;
}


sf::Vector2f PhysicalCircle::closestPointToCircle(const sf::Vector2f& circleCenter, float circleRadius, const std::vector<sf::Vector2f>& points)
{
    sf::Vector2f min_point;
    float distance_sq = INFINITY;//distance squared to speed up the computation
    float current_distance = 0.f;
    for(auto& p : points)
    {
        current_distance = MathOperations::dotProd(p - circleCenter, p - circleCenter);
        if(distance_sq > current_distance)
        {
            distance_sq = current_distance;
            min_point = p;
        }
    }

    return min_point;
}

void PhysicalCircle::computeSweptBounds(sf::Vector2f movedOffset)
{
    sweptPosition = getPosition();
    sweptPosition += movedOffset;
}

sf::Vector2f PhysicalCircle::getSweptPosition() const
{
    return sweptPosition;
}

PhysicalObjectType PhysicalCircle::getObjectType() const
{
    return PhysicalObjectType::Circle;
}
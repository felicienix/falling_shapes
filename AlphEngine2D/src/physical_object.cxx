#include "physical_object.hpp"
#include "SFML/System/Time.hpp"
#include <assert.h>

PhysicalObject::PhysicalObject()
    :fCentroid(), fLocalBounds(), forces(), torques(0.f), fLinearVelocity()
{}

PhysicalObject::~PhysicalObject()
{
}

sf::FloatRect PhysicalObject::getBoundingRectangle() const
{
       return getTransform().transformRect(fLocalBounds);
}

float PhysicalObject::getInverseInertia() const
{
    return fInverseInertia;
}

void PhysicalObject::solveCollisions(PhysicalObject& other, OverlapData overlap_data)
{
    sf::Vector2f overlap_amount = overlap_data.fOverlapAmount * overlap_data.fCollisionNormal;
        
    if(fPhysicalObjectMovementType != PhysicalObjectMovementType::Static 
        && other.fPhysicalObjectMovementType == PhysicalObjectMovementType::Static)
    {
        move(-overlap_amount);
    }
    else if(other.fPhysicalObjectMovementType != PhysicalObjectMovementType::Static 
        && fPhysicalObjectMovementType == PhysicalObjectMovementType::Static)
    {
        other.move(overlap_amount);
    }
    else if (other.fPhysicalObjectMovementType != PhysicalObjectMovementType::Static 
        && fPhysicalObjectMovementType != PhysicalObjectMovementType::Static)
    {
        move(-overlap_amount * 0.5f);
        other.move(overlap_amount * 0.5f);
    }

}


float PhysicalObject::getMass() const
{
    return fMass;
}
float PhysicalObject::getInverseMass() const
{
    return fInverseMass;
}

void PhysicalObject::setMass(float new_mass)
{
    fMass = new_mass;
    computeInertia();
}

void PhysicalObject::applyForce(const sf::Vector2f& force)
{
    forces += force;
}
void PhysicalObject::applyTorque(float torque)
{
    torques += torque;
}

void PhysicalObject::intergrateForcesAndTorque(sf::Time dt)
{
    fLinearVelocity += fInverseMass * forces * dt.asSeconds() * 0.5f;
    fAngularVelocity += fInverseInertia * torques * dt.asSeconds() * 0.5f;
}

void PhysicalObject::clearForces()
{
    forces = sf::Vector2f();
    torques = 0.f;
}

float PhysicalObject::getInertiaMomentum() const
{
    return fInertiaMomentum;
}
float PhysicalObject::getArea() const
{
    return fArea;
}
sf::Vector2f PhysicalObject::getCentroid() const
{
    return fCentroid;
}

PhysicalObjectMovementType::Type PhysicalObject::getObjectMovementType() const
{
    return fPhysicalObjectMovementType;
}

void PhysicalObject::setPhysicalObjectMovementType(PhysicalObjectMovementType::Type new_type)
{
    fPhysicalObjectMovementType = new_type;
    if(new_type == PhysicalObjectMovementType::Static)
    {
        setMass(-1.f);
        fGravitySensitive = false;
    }
}

ContactPoints::ContactPoints()
        :size(0), points{sf::Vector2f(), sf::Vector2f()}
{}

ContactPoints::ContactPoints(const ContactPoints &other)
    :size(other.size), points{other.points[0], other.points[1]}
{}

sf::Vector2f &ContactPoints::operator[](size_t index)
{
    if(index >= size)
        assert(false);
    
    return points[index];
}

void ContactPoints::clear()
{
    size = 0;
}

void ContactPoints::add(const sf::Vector2f &p)
{
    if(size >= 2) return;

    points[size++] = p;
}

void ContactPoints::remove(const sf::Vector2f &p)
{
    if(size == 0) return;

    if(points[size - 1] == p)
    {
        size -= 1;
    }
    else if(points[0] == p)
    {
        points[0] = points[1];
        size -= 1;
    }
}
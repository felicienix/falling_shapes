#include "physics_engine.hpp"
#include "maths_operations.hpp"
#include <iostream>
#define SPEED_LIMIT 1e14f

namespace
{

    //formulas are based on this incredible lecture https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf
    void add_Impulses(PhysicalObject* a, PhysicalObject* b, OverlapData data, float CONVERSION_FACTOR, float INVERSE_CONVERSION_FACTOR, sf::Time deltaTime)
    {
        float I_1 = a->getInverseInertia() * MathOperations::square(INVERSE_CONVERSION_FACTOR);  //multiplying by the conversion factor to match the 1pixel -> 1cm units
        float I_2 = b->getInverseInertia() * MathOperations::square(INVERSE_CONVERSION_FACTOR);
        float Im1 = a->getInverseMass();
        float Im2 = b->getInverseMass();
        float baumgarteFactor = 0.4f;
        float baumgarteBias = baumgarteFactor * data.fOverlapAmount * CONVERSION_FACTOR / deltaTime.asSeconds();
        
        
        float contact_factor = 1.f / (float)data.fContactPoints.size;
        for(unsigned int i = 0;i<data.fContactPoints.size;++i)
        {
            //need to convert cm to m
            sf::Vector2f r_a = (data.fContactPoints[i] - a->getPosition()) * CONVERSION_FACTOR; //multiplying by conversion factor to match the 1pixel -> 1cm units
            sf::Vector2f r_b = (data.fContactPoints[i] - b->getPosition()) * CONVERSION_FACTOR;

            float raCrossN = MathOperations::crossProduct2D(r_a, data.fCollisionNormal);
            float rbCrossN = MathOperations::crossProduct2D(r_b, data.fCollisionNormal);
            sf::Vector2f v_r = a->fLinearVelocity + MathOperations::Cross2DScalar(a->fAngularVelocity, r_a) -
             b->fLinearVelocity - MathOperations::Cross2DScalar(b->fAngularVelocity, r_b);
            
            float vr_alongNormal = MathOperations::dotProd(v_r, data.fCollisionNormal);
            if(vr_alongNormal < 0.0)
                return;
                
            float angular_addition = I_1 * MathOperations::square(raCrossN)
                + I_2 * MathOperations::square(rbCrossN);

            float e = 0.5f * (a->fRestitutionCoeff + b->fRestitutionCoeff);
            if(vr_alongNormal <= CROSS_PLATFORM_FLT_EPSILON)
                e = 0.f;

            float j = (1.f + e) * (vr_alongNormal - baumgarteBias)
                / (Im1 + Im2 + angular_addition);
            j *= contact_factor;
            j = std::max(j, 0.f);


            if(a->getObjectMovementType() == PhysicalObjectMovementType::Movable)
            {
                a->fLinearVelocity -= Im1 * (j * data.fCollisionNormal);
                a->fAngularVelocity -= I_1 * MathOperations::crossProduct2D(r_a, j * data.fCollisionNormal);
            }
            else if(a->getObjectMovementType() == PhysicalObjectMovementType::Unmovable)
            {
                a->fLinearVelocity.y -= Im1 * (j * data.fCollisionNormal.y);
            }
        
            if(b->getObjectMovementType() == PhysicalObjectMovementType::Movable)
            {
                b->fLinearVelocity += Im2 * (j * data.fCollisionNormal);
                b->fAngularVelocity += I_2 * MathOperations::crossProduct2D(r_b, j * data.fCollisionNormal);
            }
            else if(b->getObjectMovementType() == PhysicalObjectMovementType::Unmovable)
            {
                b->fLinearVelocity.y += Im2 * (j * data.fCollisionNormal).y;
            }
        
 
            /****TANGENT IMPULSE*****/

            v_r = a->fLinearVelocity + MathOperations::Cross2DScalar(a->fAngularVelocity, r_a) -
             b->fLinearVelocity - MathOperations::Cross2DScalar(b->fAngularVelocity, r_b);


            sf::Vector2f t = v_r - MathOperations::dotProd(v_r, data.fCollisionNormal) * data.fCollisionNormal;
            t = MathOperations::normalize(t);
            float raCrossT = MathOperations::crossProduct2D(r_a, t);
            float rbCrossT = MathOperations::crossProduct2D(r_b, t);

            angular_addition = I_1 * MathOperations::square(raCrossT)
                + I_2 * MathOperations::square(rbCrossT);

            
            sf::Vector2f tangent_impulse;
            
            float j_t = MathOperations::dotProd(v_r, t) / (Im1 + Im2 + angular_addition);
            j_t *= contact_factor;


            if(fabs(j_t) <= CROSS_PLATFORM_FLT_EPSILON)
                return;
            
            float mu_s = sqrt(MathOperations::square(a->fStaticFriction) + MathOperations::square(b->fStaticFriction));   //static friction
            float mu_k = sqrt(MathOperations::square(a->fDynamicFriction) + MathOperations::square(b->fDynamicFriction)); //kinetic friction
            //TODO: little bug with static friction not being used when relative velocity is low (especially with squares for whatever reason)
            if(fabs(j_t) < mu_s * j)
            {
                tangent_impulse = j_t * t;
            }
            else{
                tangent_impulse = mu_k * j * t;
            }

            if(a->getObjectMovementType() == PhysicalObjectMovementType::Movable)
            {
                a->fLinearVelocity -= Im1 * (tangent_impulse);
                a->fAngularVelocity -= I_1 * MathOperations::crossProduct2D(r_a, tangent_impulse);
            }
            if(b->getObjectMovementType() == PhysicalObjectMovementType::Movable)
            {
                b->fLinearVelocity += Im2 * (tangent_impulse);
                b->fAngularVelocity += I_2 * MathOperations::crossProduct2D(r_b, tangent_impulse);
            }

        }
    }


    //formulas are based on this incredible lecture https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf
    void modify_velocities(PhysicalObject* a, PhysicalObject* b, OverlapData data, float CONVERSION_FACTOR, float INVERSE_CONVERSION_FACTOR, sf::Time deltaTime)
    {
        add_Impulses(a, b, data, CONVERSION_FACTOR, INVERSE_CONVERSION_FACTOR, deltaTime);
    }
}

PhysicsEngine::PhysicsEngine(unsigned int sub_steps)
    :fObjects(), fObjectsByType(), fGravitySensitiveObjects(), fCollisionPairs(), fSubsteps(sub_steps)
{
}

PhysicsEngine::~PhysicsEngine()
{
    for(auto* obj : fObjects)
    {
        if(obj)
            delete obj;
    }
}


std::vector<PhysicalObject*>::iterator PhysicsEngine::begin()
{
    return fObjects.begin();
}
std::vector<PhysicalObject*>::iterator PhysicsEngine::end()
{
    return fObjects.end();
}

std::vector<PhysicalObject*>::const_iterator PhysicsEngine::begin() const
{
    return fObjects.begin();
}
std::vector<PhysicalObject*>::const_iterator PhysicsEngine::end() const
{
    return fObjects.end();
}


std::list<PhysicalObject*>& PhysicsEngine::getGravitySensitiveObjects()
{
    return fGravitySensitiveObjects;
}

std::list<PhysicalObject *> &PhysicsEngine::getPhysicalObjectsByMovementType(PhysicalObjectMovementType::Type type)
{
    return fObjectsByType[type];
}

void PhysicsEngine::addObject(PhysicalObject* obj)
{
    fObjects.push_back(obj);
    fObjectsByType[obj->getObjectMovementType()].push_back(obj);
    if(obj->fGravitySensitive)
        fGravitySensitiveObjects.push_back(obj);
    obj->computeSweptBounds(sf::Vector2f());
}

void PhysicsEngine::removeObject(PhysicalObject *obj)
{
    fGravitySensitiveObjects.remove(obj);
    fObjectsByType[obj->getObjectMovementType()].remove(obj);
    auto found = std::find(fObjects.begin(), fObjects.end(), obj);
    if(found != fObjects.end())
    {
        fObjects.erase(found);
    }
}

void PhysicsEngine::removeGravityOnObject(PhysicalObject* obj)
{
    obj->fGravitySensitive = false;
    fGravitySensitiveObjects.remove(obj);
}

void PhysicsEngine::setPixelToMetersConversionFactor(float newConversionFactor)
{
    CONVERSION_FACTOR = newConversionFactor;
    INVERSE_CONVERSION_FACTOR = 1.f / newConversionFactor;
}

float PhysicsEngine::getPixelToMetersConversionFactor() const
{
    return CONVERSION_FACTOR;
}

float PhysicsEngine::getInversePixelToMetersConversionFactor() const
{
    return INVERSE_CONVERSION_FACTOR;
}

void PhysicsEngine::addGravityOnObject(PhysicalObject* obj)
{
    if(!obj->fGravitySensitive)
    {
        obj->fGravitySensitive = true;
        fGravitySensitiveObjects.push_back(obj);
    }
}

void PhysicsEngine::update(sf::Time dt)
{       
    sf::Time sub_dt = dt / (float)(fSubsteps);

    applyGravity();

    for(unsigned int step = 0; step < fSubsteps;++step)
    {
        fCollisionPairs.clear();

        integrateForces(sub_dt);
        computeSweptBounds(sub_dt);
        //the solving happens inside the collision detection functions
        computeCollisions();
        updateVelocitiesWithImpulses(sub_dt);
        updatePositions(sub_dt);
    }

    clearForces();
}

void PhysicsEngine::integrateForces(sf::Time dt)
{
    for(size_t objectType = PhysicalObjectMovementType::Unmovable; objectType <PhysicalObjectMovementType::MovementTypeCount; ++objectType)
    {
        for(auto* obj : fObjectsByType[objectType])
        {
            obj->intergrateForcesAndTorque(dt);
        }
    }
}

void PhysicsEngine::clearForces()
{
    for(size_t objectType = PhysicalObjectMovementType::Unmovable; objectType <PhysicalObjectMovementType::MovementTypeCount; ++objectType)
    {
        for(auto* obj : fObjectsByType[objectType])
        {
            obj->clearForces();
        }
    }
}

void PhysicsEngine::computeSweptBounds(sf::Time dt)
{
    for(size_t objectType = PhysicalObjectMovementType::Unmovable; objectType <PhysicalObjectMovementType::MovementTypeCount; ++objectType)
    {
        for(auto* obj : fObjectsByType[objectType])
        {
            obj->computeSweptBounds(obj->fLinearVelocity * dt.asSeconds());
        }
    }
}

void PhysicsEngine::applyGravity()
{
    for(auto* obj : fGravitySensitiveObjects)
    {
        obj->applyForce(obj->getMass() * INVERSE_CONVERSION_FACTOR * sf::Vector2f(0.f, fGravityConstant)); //multiplying by conversion factor to match the 1pixel -> 1cm units   
    }
}

void PhysicsEngine::computeCollisions()
{
    OverlapData overlap_data;
    auto& unmovableList = fObjectsByType[PhysicalObjectMovementType::Unmovable];
    auto& movableList = fObjectsByType[PhysicalObjectMovementType::Movable];
    //first test collision with static objects
    for(size_t objectType = PhysicalObjectMovementType::Unmovable; objectType <PhysicalObjectMovementType::MovementTypeCount; ++objectType)
    {
        for(auto* obj : fObjectsByType[objectType])
        {
            for(auto* static_obj : fObjectsByType[PhysicalObjectMovementType::Static])
            {
                if(obj->doesCollide(*static_obj, overlap_data))
                {
                    fCollisionPairs.push_front(CollisionPair(obj, static_obj, overlap_data));
                }
            }
        }
    }

    //then test collision between every movable and unmovable objects    
    for(auto* movable_obj : fObjectsByType[PhysicalObjectMovementType::Movable])
    {
        for(auto* unmovable_obj : fObjectsByType[PhysicalObjectMovementType::Unmovable])
        {
            if(movable_obj->fInteractionType == PhysicalObjectInteractionType::WithEverything 
                && unmovable_obj->fInteractionType == PhysicalObjectInteractionType::WithEverything 
                && movable_obj->doesCollide(*unmovable_obj, overlap_data))
            {
                fCollisionPairs.push_front(CollisionPair(movable_obj, unmovable_obj, overlap_data));
            }
        }

    }

    //then test collision between every movable
    for(auto iter_movable = fObjectsByType[PhysicalObjectMovementType::Movable].begin();iter_movable != fObjectsByType[PhysicalObjectMovementType::Movable].end(); ++iter_movable)
    {
        for(auto iter_other_movable = iter_movable;++iter_other_movable != fObjectsByType[PhysicalObjectMovementType::Movable].end();)
        {
            if((*iter_movable)->fInteractionType != PhysicalObjectInteractionType::OnlyStatic 
                && (*iter_other_movable)->fInteractionType != PhysicalObjectInteractionType::OnlyStatic
                && (*iter_movable)->doesCollide(*(*iter_other_movable), overlap_data))
            {
                fCollisionPairs.push_front(CollisionPair(*iter_movable, *iter_other_movable, overlap_data));
            }
        }
    }

    //and finally test collision between every unmovable
    for(auto iter_unmovable = fObjectsByType[PhysicalObjectMovementType::Unmovable].begin();iter_unmovable != fObjectsByType[PhysicalObjectMovementType::Unmovable].end(); ++iter_unmovable)
    {
        for(auto iter_other_unmovable = iter_unmovable;++iter_other_unmovable != fObjectsByType[PhysicalObjectMovementType::Unmovable].end();)
        {
            if((*iter_unmovable)->fInteractionType != PhysicalObjectInteractionType::OnlyStatic 
                && (*iter_other_unmovable)->fInteractionType != PhysicalObjectInteractionType::OnlyStatic
                && (*iter_unmovable)->doesCollide(*(*iter_other_unmovable), overlap_data))
            {
                fCollisionPairs.push_front(CollisionPair(*iter_unmovable, *iter_other_unmovable, overlap_data));
            }
        }
    }
}

void PhysicsEngine::updatePositions(sf::Time dt)
{
    for(size_t objectType = PhysicalObjectMovementType::Unmovable; objectType <PhysicalObjectMovementType::MovementTypeCount; ++objectType)
    {
        for(auto* obj : fObjectsByType[objectType])
        {
            if(MathOperations::dotProd(obj->fLinearVelocity, obj->fLinearVelocity) < SPEED_LIMIT 
            && !std::isnan(abs(obj->fLinearVelocity.x)) && !std::isnan(abs(obj->fLinearVelocity.y)))
            {
                if(fabs(obj->fLinearVelocity.x) <= (float)fSubsteps)
                    obj->fLinearVelocity.x = 0.f; //weird fix for the squares when standing still
                obj->move(obj->fLinearVelocity * dt.asSeconds());
                obj->rotate(obj->fAngularVelocity * dt.asSeconds());
            }
            else
            {
                obj->fLinearVelocity.x = 0.f;
                obj->fLinearVelocity.y = 0.f;
                obj->fAngularVelocity = 0.f;
            }
        }
    }
}
void PhysicsEngine::updateVelocitiesWithImpulses(sf::Time deltaTime)
{
    for(auto& pair : fCollisionPairs)
    {
        modify_velocities(pair.a, pair.b, pair.data, CONVERSION_FACTOR, INVERSE_CONVERSION_FACTOR, deltaTime);
    }
}
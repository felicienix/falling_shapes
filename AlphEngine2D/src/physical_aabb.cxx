#include "physical_aabb.hpp"
#include "physical_circle.hpp"
#include "polygon.hpp"
#include "SFML/Graphics/RectangleShape.hpp"

PhysicalAABB::PhysicalAABB()
    :PhysicalObject()
{
}

PhysicalAABB::PhysicalAABB(float _width, float _height)
    :PhysicalObject()
{
    fLocalBounds.width = _width;
    fLocalBounds.height = _height;
    fLocalBounds.left = -_width * 0.5f;
    fLocalBounds.top = -_height * 0.5f;
    fArea = _width * _height;
    fCentroid.x = _width * 0.5f;
    fCentroid.y = _height * 0.5f;
    computeInertia();
    setOrigin(fCentroid);
}

PhysicalAABB::~PhysicalAABB()
{
}

sf::FloatRect PhysicalAABB::getBoundingRectangle() const
{
    return sf::FloatRect(sweptPosition + sf::Vector2f(fLocalBounds.left, fLocalBounds.top), sf::Vector2f(fLocalBounds.width, fLocalBounds.height));
}

void PhysicalAABB::setWidthHeight(float new_width, float new_height)
{
    fLocalBounds.width = new_width;
    fLocalBounds.height = new_height;
    fLocalBounds.left = -new_width * 0.5f;
    fLocalBounds.top = -new_height * 0.5f;
    fArea = new_width * new_height;
    fCentroid.x = new_width * 0.5f;
    fCentroid.y = new_height * 0.5f;
    computeInertia();
    setOrigin(fCentroid);

}

void PhysicalAABB::setWidthHeight(const sf::Vector2f &new_dimensions)
{
    setWidthHeight(new_dimensions.x, new_dimensions.y);
}

sf::Vector2f PhysicalAABB::getWidthHeight() const
{
    return sf::Vector2f(fLocalBounds.width, fLocalBounds.height);
}

bool PhysicalAABB::doesCollide(PhysicalObject &other, OverlapData &overlap_data)
{
    switch (other.getObjectType())
    {
    case PhysicalObjectType::AABB:
    {
        PhysicalAABB& other_aabb = static_cast<PhysicalAABB&>(other);
        return doesCollideWithAABB(other_aabb, overlap_data);   
    }
        break;
    case PhysicalObjectType::Circle:
    {
        PhysicalCircle& other_circle = static_cast<PhysicalCircle&>(other);
        return doesCollideWithCircle(other_circle, overlap_data);   
    } 
        break;
    case PhysicalObjectType::Polygon:
    {
        Polygon& poly_other = static_cast<Polygon&>(other);
        if(other.doesCollide(*this, overlap_data))
        {
            overlap_data.fCollisionNormal *= -1.f;
            return true;
        }
    
        return false;
    }
        break;
    
    default:
        break;
    }

    return false;
}

void PhysicalAABB::computeInertia()
{
    if(fMass < 0.f || fPhysicalObjectMovementType == PhysicalObjectMovementType::Static)
    {
        fInverseMass = 0.f;
    }
    else
    {
        fInverseMass = 1.f / fMass;
    }

    fInertiaMomentum = std::numeric_limits<float>::max();
    fInverseInertia = 0.f;
}

void PhysicalAABB::computeSweptBounds(sf::Vector2f movedOffset)
{
    sweptPosition = getPosition();
    sweptPosition += movedOffset;
}

sf::Vector2f PhysicalAABB::getSweptPosition() const
{
    return sweptPosition;
}

PhysicalObjectType PhysicalAABB::getObjectType() const
{
    return PhysicalObjectType::AABB;
}

bool PhysicalAABB::containsPoint(const sf::Vector2f &point)
{
    return getBoundingRectangle().contains(point);
}

PhysicalObject *PhysicalAABB::clonePhysicalObject() const
{
    return new PhysicalAABB(*this);
}

std::unique_ptr<sf::Drawable> PhysicalAABB::getVisualMesh() const
{
    auto ptr_drawable = std::make_unique<sf::RectangleShape>(sf::Vector2f(fLocalBounds.width, fLocalBounds.height));
    ptr_drawable.get()->setFillColor(sf::Color::Transparent);
    ptr_drawable.get()->setOutlineColor(sf::Color::Blue);
    ptr_drawable.get()->setOutlineThickness(1.f);
    ptr_drawable.get()->setOrigin(fCentroid);
    ptr_drawable.get()->setPosition(getPosition());

    return std::move(ptr_drawable);
}

bool PhysicalAABB::doCircleAndAABBCollide(const sf::Vector2f &circleCenter, float circleRadius, const sf::FloatRect &aabbRectangle, OverlapData &data)
{
    //compute the distance between the centers
    sf::Vector2f aabbCenter = 
        sf::Vector2f(aabbRectangle.left, aabbRectangle.top) + sf::Vector2f(aabbRectangle.width * 0.5f, aabbRectangle.height * 0.5f);
    sf::Vector2f centerDistance = aabbCenter - circleCenter;
    sf::Vector2f clampDistance;
    //clamp that distance to the bounding box of the AABB
    clampDistance.x = std::clamp(centerDistance.x, -aabbRectangle.width * 0.5f, aabbRectangle.width * 0.5f);
    clampDistance.y = std::clamp(centerDistance.y, -aabbRectangle.height * 0.5f, aabbRectangle.height * 0.5f);
    //compute the closest point to AABB's center
    sf::Vector2f closestPoint = aabbCenter - clampDistance;
    //check whether the closest point is in the circle
    float pointToCircleCenterLength = MathOperations::length(closestPoint - circleCenter);
    if(pointToCircleCenterLength < circleRadius)
    {
        data.fContactPoints.clear();
        data.fOverlapAmount = circleRadius - pointToCircleCenterLength;
        data.fContactPoints.add(closestPoint);

        return true;
    }


    return false;
}

bool PhysicalAABB::doesCollideWithCircle(PhysicalCircle &other, OverlapData &data)
{
    data.fContactPoints.clear();
    //compute the distance between the centers
    sf::Vector2f centerDistance = sweptPosition - other.getSweptPosition();
    sf::Vector2f clampDistance;
    //clamp that distance to the bounding box of the AABB
    clampDistance.x = std::clamp(centerDistance.x, -fLocalBounds.width * 0.5f, fLocalBounds.width * 0.5f);
    clampDistance.y = std::clamp(centerDistance.y, -fLocalBounds.height * 0.5f, fLocalBounds.height * 0.5f);
    //compute the closest point to AABB's center
    sf::Vector2f closestPoint = sweptPosition - clampDistance;
    //check whether the closest point is in the circle
    float pointToCircleCenterLength = MathOperations::length(closestPoint - other.getSweptPosition());
    if(pointToCircleCenterLength < other.getRadius())
    {
        data.fOverlapAmount = other.getRadius() - pointToCircleCenterLength;
        data.fContactPoints.add(closestPoint);
        data.fCollisionNormal = -MathOperations::normalize(closestPoint - other.getSweptPosition());
        solveCollisions(other, data);
        return true;
    }

    return false;
}

void computeNormalAndContactPointsWidth(const sf::FloatRect& thisBox, const sf::FloatRect& otherBox, const sf::FloatRect& intersection, OverlapData& data)
{
    if(MathOperations::equalFloat(intersection.left, thisBox.left)) //otherbox is on the left side of thisbox
    {
        data.fCollisionNormal = sf::Vector2f(-1.f, 0.f);
        data.fContactPoints.add(sf::Vector2f(intersection.left, intersection.top));
        data.fContactPoints.add(sf::Vector2f(intersection.left, intersection.top + intersection.height));
    }
    else    //otherbox is on the right side of thisbox
    {
        data.fCollisionNormal = sf::Vector2f(1.f, 0.f);
        data.fContactPoints.add(sf::Vector2f(intersection.left + intersection.width, intersection.top));
        data.fContactPoints.add(sf::Vector2f(intersection.left + intersection.width, intersection.top + intersection.height));
    }
}

void computeNormalAndContactPointsHeight(const sf::FloatRect& thisBox, const sf::FloatRect& otherBox, const sf::FloatRect& intersection, OverlapData& data)
{
    if(MathOperations::equalFloat(intersection.top, thisBox.top)) //otherbox is on top of thisbox
    {
        data.fCollisionNormal = sf::Vector2f(0.f, -1.f);
        data.fContactPoints.add(sf::Vector2f(intersection.left, intersection.top));
        data.fContactPoints.add(sf::Vector2f(intersection.left + intersection.width, intersection.top));
    }
    else    //otherbox is below thisbox
    {
        data.fCollisionNormal = sf::Vector2f(0.f, 1.f);
        data.fContactPoints.add(sf::Vector2f(intersection.left, intersection.top + intersection.height));
        data.fContactPoints.add(sf::Vector2f(intersection.left + intersection.width, intersection.top + intersection.height));
    }
    
}

bool PhysicalAABB::doAABBAndAABBCollide(const sf::FloatRect &aabbRectangle1, const sf::FloatRect &aabbRectangle2, OverlapData &data)
{
    sf::FloatRect intersection;
    if(aabbRectangle1.intersects(aabbRectangle2, intersection))
    {
        data.fContactPoints.clear();
        if(intersection.width < intersection.height)    //this means it's colliding on the side (right or left)
        {
            data.fOverlapAmount = intersection.width;
            data.fCollisionNormal = sf::Vector2f(1.f,0.f);
        }
        else    //this means it's colliding on top (or bottom)
        {
            data.fOverlapAmount = intersection.height;
            data.fCollisionNormal = sf::Vector2f(0.f,1.f);
        }
        
        return true;
    }

    return false;
}

bool PhysicalAABB::doesCollideWithAABB(PhysicalAABB &other, OverlapData &data)
{
    sf::FloatRect intersection;
    sf::FloatRect thisBox = getBoundingRectangle();
    sf::FloatRect otherBox = other.getBoundingRectangle();
    if(thisBox.intersects(otherBox, intersection))
    {
        data.fContactPoints.clear();
        if(intersection.width < intersection.height)    //this means it's colliding on the side (right or left)
        {
            computeNormalAndContactPointsWidth(thisBox, otherBox, intersection, data);
            data.fOverlapAmount = intersection.width;
        }
        else    //this means it's colliding on top (or bottom)
        {
            computeNormalAndContactPointsHeight(thisBox, otherBox, intersection, data);
            data.fOverlapAmount = intersection.height;
        }
        
        solveCollisions(other, data);
        return true;
    }

    return false;

}
